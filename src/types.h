#define MAXLEN 16

typedef struct {
    char id[MAXLEN];
} ent_t;

typedef struct entl_ {
    ent_t ent;
    struct entl_ *next;
} entl_t;

typedef struct {
    ent_t* ent_1;
    ent_t* ent_2;
    char id[MAXLEN];
} rel_t;

typedef struct rell_ {
    rel_t rel;
    struct rell_ *next;
} rell_t;
