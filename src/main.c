#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "types.h"


int main(int argc, char* argv[]) {
    int running = 1;
    char buff[64];
    char* tok;
    while (running) {
        scanf("%s", buff);
        tok = strtok(buff, " ");

        if (!strcmp(tok, "end")) {
            running = 0;
        }

    };
    return 0;
}